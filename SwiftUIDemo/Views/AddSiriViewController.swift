//
//  AddSiriViewController.swift
//  SwiftUIDemo
//
//  Created by Gaurang Vyas on 19/09/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import Foundation

import SwiftUI
import Combine
import IntentsUI
import IntentsUI.INUIAddVoiceShortcutButton

struct AddSiriConroller: UIViewControllerRepresentable {
    
    var siriController:INUIAddVoiceShortcutViewController
    
   func makeCoordinator() -> Coordinator {
       Coordinator(self)
   }
   
   func makeUIViewController(context: Context) -> INUIAddVoiceShortcutViewController {
       return siriController
   }
   
   func updateUIViewController(_ uiViewController: INUIAddVoiceShortcutViewController, context: Context) {
       
   }
    
    class Coordinator: NSObject, INUIAddVoiceShortcutViewControllerDelegate {
        
        var parent: AddSiriConroller
        
        init(_ imagePickerController: AddSiriConroller) {
            self.parent = imagePickerController
        }
        
        func addVoiceShortcutViewController(_ controller: INUIAddVoiceShortcutViewController, didFinishWith voiceShortcut: INVoiceShortcut?, error: Error?) {
            
        }
        
        func addVoiceShortcutViewControllerDidCancel(_ controller: INUIAddVoiceShortcutViewController) {
            
        }
        
    }
}
