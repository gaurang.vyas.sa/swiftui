//
//  RecipeMesureView.swift
//  SwiftUIDemo
//
//  Created by Gaurang Vyas on 12/09/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import SwiftUI

struct RecipeMesureView: View {
    let preparationTime: String?
    let complexity: String?
    let serves: String?
    
    var body: some SwiftUI.View {
        HStack(spacing: 30){
            IconTextStack(icon: #imageLiteral(resourceName: "img_clock"), text: preparationTime ?? "")
            IconTextStack(icon: #imageLiteral(resourceName: "img_bars"), text: complexity ?? "")
            IconTextStack(icon: #imageLiteral(resourceName: "img_serve"), text: serves ?? "")
        }
        .padding(EdgeInsets(top: 0, leading: 12, bottom: 12, trailing: 12))
    }
}
