//
//  ContentView.swift
//  SwiftUIDemo
//
//  Created by Gaurang Vyas on 03/09/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import SwiftUI
import Moya
import NVActivityIndicatorView
import Combine

struct LoginView: View {
    @State private var email: String = "jm1@example.com"
    @State private var password: String = "jay@123"
   
    let userProvider = MoyaProvider<UserServices>(plugins: [NetworkLoggerPlugin(verbose: true)])
    var body: some View {
        GeometryReader { geometry in
            VStack {
                VStack(alignment: .center, spacing: 4){
                    Text("LOG IN").font(.largeTitle)
                    Text("Good to see you again").font(.caption)
                }.frame( height: geometry.size.height / 2.2)
            
                TextField(Messages.yourEmail, text: self.$email)
                    .modifier(PrimaryTextFieldStyle()).padding(.bottom, 10)
                
                SecureField(Messages.password, text: self.$password)
                .modifier(PrimaryTextFieldStyle()).padding(.bottom, 22)
            
                Button(action: {
                     self.loginTapped()
                }) {
                    Text(Strings.login)
                }.buttonStyle(PrimaryButtonStyle()).padding(.bottom, 16)
                
                Button(action: {
                    
                }) {
                    Text("Forgot your password?")
                        .font(.subheadline)
                        .foregroundColor(Color.black)
                }
                Spacer()
            }.padding(16)
        }
    }
    
    private func loginTapped() {
    
        if email.isEmpty || password.isEmpty {
            return
        }
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        userProvider.request(.login(email: email, password: password)) { result in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            switch result{
            case let .success(response):
                do{
                    let loginInfo = try JSONDecoder().decode(LoginInfo.self, from: response.data)
                    if let error = loginInfo.error{
                       print("error",error)
                    }else{
                        print(loginInfo)
                        UserData.doLogin(info: loginInfo)
                    }
                }catch let error{
                    print(error)
                }
                break
            case let .failure(error):
                print(Strings.failure,error)
                break
            }
        }
    }
}
#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
#endif
