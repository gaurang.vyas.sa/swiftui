//
//  RecipeDetailsView.swift
//  SwiftUIDemo
//
//  Created by Gaurang Vyas on 11/09/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import SwiftUI

struct RecipeDetailsView: SwiftUI.View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var viewModel = RecipeDetailsViewModel()
    var recipeId: Int
    
    init(_ recipeId: Int) {
        self.recipeId = recipeId
        viewModel.recipeId = recipeId
        
    }
    
    var body: some SwiftUI.View{
        let recipe = viewModel.recipe
        
        return VStack(alignment: .leading, spacing: 10){
            if recipe != nil{
                
                URLImage(imageURL: viewModel.recipe?.photo)
                .frame(height: 200)
                .clipped()
                
                ScrollView(.vertical, showsIndicators: true){
                    
                    VStack(alignment: .center, spacing: nil) {
                        Text("SALAD").font(.caption)
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        Text(recipe?.name ?? "").font(.body)
                        RecipeMesureView(preparationTime: recipe?.preparationTime, complexity: recipe?.complexity, serves: recipe?.serves)
                    }
                    ForEach(viewModel.sectionArray,id: \.id) { info in
                        IngredientsView(section: info)
                    }
                }
                .frame(minWidth: 0, maxWidth: .infinity, alignment: Alignment.leading)
            }
        }
        .background(Color(.systemGroupedBackground))
        .onAppear(perform: loadData)
        .edgesIgnoringSafeArea(.top)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: btnBack)
        
    }
    
    var btnBack : some SwiftUI.View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
        }) {
            HStack {
            Image("img_back") // set image here
                .aspectRatio(contentMode: .fit)
                .foregroundColor(.white)
            }
        }
    }
    
    private func loadData(){
        viewModel.load()
    }
    
    struct IngredientsView: View{
        var section:RecipeDetailsSections
        var body: some View{
            
            Section(header: Text(section.title.uppercased())
                .font(.caption)
                .frame(height: 40)) {
                    
                ForEach(0..<section.textArray.count) { i in
                    HStack( spacing: 8) {
                        if self.section.isIngredients{
                            Text("\(i+1)")
                                .font(.caption)
                                .frame(width: 14, height: 14)
                                .background(Color.orange)
                                .clipShape(Circle())
                                .foregroundColor(.white)
                        }else{
                            Circle().fill(Color.orange).frame(width: 8, height: 8, alignment: .leading)
                                
                        }
                        Text(self.section.textArray[i])
                        .font(.subheadline)
                        .frame(minWidth: 0, maxWidth: .infinity, alignment: Alignment.leading)
                        .multilineTextAlignment(.leading)
                    }
                    .frame(height: 30)
                    .background(Color.white)
                }
            }
        }
    }
}

