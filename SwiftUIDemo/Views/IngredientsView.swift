//
//  IngredintsView.swift
//  SwiftUIDemo
//
//  Created by Gaurang Vyas on 06/09/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import SwiftUI
import Moya

struct Ingredients: View {
    
    @ObservedObject var ingredientsModel = IngredientViewModel()
    var recipeId:Int = 0
    @State var showingInput: Bool = false
    @State var ingredintName: String = ""

    init(recipeId: Int) {
        self.recipeId = recipeId
        
    }
    
    var body: some View {
        Form {
            if showingInput{
                Section{
                    TextField("Enter ingrediant", text: $ingredintName)
                }
            }
            Section {
                List{
                    ForEach(ingredientsModel.ingredientArray,id: \.id) { info in
                        Text(info.ingredient ?? "")
                    }
                .onDelete(perform: delete(at:))
                }
            }
        }
        
        .navigationBarTitle(Text(ViewControllerTitle.Ingredients), displayMode: .inline)
        .navigationBarItems(trailing: Button(action: {
            self.save()
            self.showingInput = !self.showingInput
        }, label: {
            Text(showingInput ? "Save" : "Add")
        }))
        .onAppear(perform: fetch)
    }
    
    func delete(at offsets: IndexSet) {
        ingredientsModel.delete(index: offsets.first!)
    }
    
    private func fetch() {
        print("Recipe ID Ingredients", self.recipeId)
        ingredientsModel.recipeId = recipeId
        ingredientsModel.fetch()
    }
    
    private func save(){
        let name = ingredintName
        ingredientsModel.add(string: name)
        print(name)
    }
    
    
}
