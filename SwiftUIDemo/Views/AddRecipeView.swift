//
//  AddRecipeView.swift
//  SwiftUIDemo
//
//  Created by Gaurang Vyas on 04/09/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import SwiftUI
import Combine
import Moya

struct AddRecipeView: View {
    let feedsProvider = MoyaProvider<ReceipesServices>(plugins: [NetworkLoggerPlugin(verbose: true)])
    var feedsArray: [RecipeInfo] = []
    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        return formatter
    }
    var recipeId: Int
    @State private var name: String = ""
    @State private var durationString: String = ""
    let complexity = [Strings.easy, Strings.medium, Strings.complex]
    @State private var selectedComplexity = 0
    @State private var serves: Int = 1
    @State var showingPicker = false
    var body: some View {
        Form {
            
            Section(header: Text("Recipe Name")) {
                TextField("Enter Name", text: $name)
            }
            
            Section(header: Text(Strings.duration)) {
                TextField(Strings.duration, text: $durationString)
            }
                
            Section {
                Picker(selection: $selectedComplexity, label: Text(Strings.complexity)) {
                    ForEach(0 ..< complexity.count) {
                        Text(self.complexity[$0])
                    }
                }
                Stepper(value: $serves, in: 1...10) {
                    Text("Serves: \(serves) \(serves == 1 ? Strings.people : Strings.peoples)")
                }
            }
            
            Section{
                Button("Show image picker") {
                    self.showingPicker = true
                }.sheet(isPresented: $showingPicker,
                        onDismiss: {
                            self.showingPicker = false
                        }, content: {
                            ImagePicker.shared.view
                        })
                .onReceive(ImagePicker.shared.$image) { image in
                    
                }
            }
            
            Section{
                NavigationLink(destination: Ingredients(recipeId: recipeId)) {
                    Text(Strings.ingredints)
                }
                NavigationLink(destination: InstructionView()) {
                    Text(Strings.instruction)
                }
            }
        }
    .navigationBarTitle("Add Recipe")
        .onAppear {
            self.fetch()
        }
    }
    
    private func fetch(){
        print("Recipe ID", self.recipeId)
    }
}


