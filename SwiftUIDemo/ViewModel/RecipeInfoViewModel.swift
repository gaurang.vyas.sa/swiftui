//
//  RecipeInfoViewModel.swift
//  SwiftUIDemo
//
//  Created by Gaurang Vyas on 04/09/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import Moya

public class RecipeInfoViewModel: ObservableObject {
    let feedsProvider = MoyaProvider<ReceipesServices>(plugins: [NetworkLoggerPlugin(verbose: true)])
    public let willChange = PassthroughSubject<RecipeInfoViewModel, Never>()
    
    @Published var recipes: [RecipeInfo] = [RecipeInfo]() {
        didSet {
            willChange.send(self)
        }
    }
    
    func load() {
        feedsProvider.request(.feeds) { result in
            switch result{
            case let .success(response):
                do{
                    let info = try JSONDecoder().decode([RecipeInfo].self, from: response.data)
                    self.recipes = info.filter({$0.photo != nil})
                }catch let error{
                    print(error)
                }
                break
            case let .failure(error):
                print(Strings.failure,error)
                break
            }
        }
    }
}

