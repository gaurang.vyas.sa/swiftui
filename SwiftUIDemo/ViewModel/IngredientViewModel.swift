//
//  IngredientViewModel.swift
//  SwiftUIDemo
//
//  Created by Gaurang Vyas on 06/09/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import Moya
import NVActivityIndicatorView

public class IngredientViewModel: ObservableObject {
    var recipeId:Int = 0
    let recipeProvider = MoyaProvider<ReceipesServices>(plugins: [NetworkLoggerPlugin(verbose: true)])
    public let willChange = PassthroughSubject<IngredientViewModel, Never>()
    
    @Published var ingredientArray: [Ingredient] = [Ingredient]() {
        didSet {
            willChange.send(self)
        }
    }
    
    func fetch() {
        print("sdff",recipeId)
        recipeProvider.request(.getIngredients(id: recipeId)) { result in
            switch result{
            case let .success(response):
                do{
                    let info = try JSONDecoder().decode([Ingredient].self, from: response.data)
                    self.ingredientArray = info
                }catch let error{
                    print(error)
                }
                break
            case let .failure(error):
                print(Strings.failure,error)
                break
            }
        }
    }
    
    func add(string: String) {
        let params: [String:Any] = [WSParams.recipeId: recipeId, WSParams.ingredient: string]
        recipeProvider.request(.addIngredient(params: params)) { result in
            switch result{
            case let .success(response):
                do{
                    let info = try JSONDecoder().decode(CommonResponse.self, from: response.data)
                    if let message = info.msg{
                        print(message)
                    }
                    self.fetch()
                }catch let error{
                    print(error)
                }
                break
            case let .failure(error):
                print(Strings.failure,error)
                break
            }
        }
    }
    
    func delete(index:Int) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        let params: [String:Any] = [WSParams.recipeId: recipeId, WSParams.ingredientId: ingredientArray[index].id]
        recipeProvider.request(.removeIngredient(params: params)) { result in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            switch result{
            case let .success(response):
                do{
                    let info = try JSONDecoder().decode(CommonResponse.self, from: response.data)
                    if let message = info.msg{
                        print(message)
                        self.ingredientArray.remove(at: index)
                    }else{
                        self.fetch()
                    }
                }catch let error{
                    print(error)
                }
                break
            case let .failure(error):
                print(Strings.failure,error)
                break
            }
        }
    }
}
