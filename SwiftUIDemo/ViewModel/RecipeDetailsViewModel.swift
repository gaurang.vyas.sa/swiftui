//
//  RecipeDetailsViewModel.swift
//  SwiftUIDemo
//
//  Created by Gaurang Vyas on 11/09/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import Moya

public class RecipeDetailsViewModel: ObservableObject {
    var recipeId:Int = 0
    let feedsProvider = MoyaProvider<ReceipesServices>(plugins: [NetworkLoggerPlugin(verbose: true)])
    public let willChange = PassthroughSubject<RecipeDetailsViewModel, Never>()
    var sectionArray: [RecipeDetailsSections] = []
    
    @Published var recipe: RecipeDetails? {
        didSet {
            if recipe != nil {
                sectionArray.removeAll()
                if let ingredients = recipe?.ingredients, !ingredients.isEmpty{
                           var textArray: [String?] = []
                           for row in ingredients{
                               textArray.append(row.ingredient)
                           }
                           self.sectionArray.append(RecipeDetailsSections(title: LabelTitles.ingredients, isIngredients: true, textArray: textArray.compactMap({$0})))
                }
                if let instrictions = recipe?.instructions, !instrictions.isEmpty{
                    var textArray: [String?] = []
                    for row in instrictions{
                        textArray.append(row.instruction)
                    }
                    self.sectionArray.append(RecipeDetailsSections(title: LabelTitles.instructions, isIngredients: false, textArray: textArray.compactMap({$0})))
                }
            }
            willChange.send(self)
        }
    }
    
    
    func load() {
        feedsProvider.request(.details(id: recipeId)) { result in
            switch result{
            case let .success(response):
                do{
                    let info = try JSONDecoder().decode(RecipeDetails.self, from: response.data)
                    self.recipe = info
                }catch let error{
                    print(error)
                }
                break
            case let .failure(error):
                print(Strings.failure,error)
                break
            }
        }
    }
}


