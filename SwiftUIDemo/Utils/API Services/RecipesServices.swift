//
//  RecipesServices.swift
//  My Recipes
//
//  Created by Gaurang Vyas on 29/08/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import Foundation

import Moya

enum ReceipesServices {
    case feeds
    case details(id: Int)
    case add(params: [String:Any])
    case addRecipePhoto(recipeId: Int, photo: UIImage)
    case getIngredients(id:Int)
    case addIngredient(params: [String:Any])
    case removeIngredient(params : [String:Any])
    case getinstructions(id:Int)
    case addInstruction(params: [String:Any])
    case removeInstruction(params : [String:Any])
}

extension ReceipesServices: TargetType {
    var task: Task {
        switch self {
        case .feeds, .details, .getIngredients, .getinstructions:
            return .requestPlain
        case .add(let params):
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        case .addRecipePhoto(let recipeId, let photo):
            let imageData = photo.jpegData(compressionQuality: 1)
            let recipeIdData = String(recipeId).data(using: String.Encoding.utf8) ?? Data()
            var formData: [Moya.MultipartFormData] = [Moya.MultipartFormData(provider: .data(imageData!), name:WSParams.photo, fileName: String.randomString(length: 10) + ".jpeg", mimeType: "image/jpeg")]
            formData.append(Moya.MultipartFormData(provider: .data(recipeIdData), name: WSParams.recipeId))
            return .uploadMultipart(formData)
        case .addIngredient(let params):
             return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        case .removeIngredient(let params):
             return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        case .addInstruction(let params):
             return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        case .removeInstruction(let params):
             return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return [WSParams.authorization: UserData.token, WSParams.contentType: WSParams.applicationJson]
    }
    
    var baseURL: URL { return URL(string: URLPath.APIBaseURL + URLPath.recipe)! }
    
    var path: String {
        switch self {
        case .feeds:
            return URLPath.feeds
        case .details(let id):
            return "\(id)/\(URLPath.recipeDetails)"
        case .add:
            return URLPath.addRecipe
        case .addRecipePhoto:
            return URLPath.addRecipePhoto
        case .getIngredients(let id):
            return "\(id)/\(URLPath.ingredients)"
        case .addIngredient:
            return URLPath.addIngredients
        case .removeIngredient:
            return URLPath.removeIngredients
        case .getinstructions(let id):
            return "\(id)/\(URLPath.instructions)"
        case .addInstruction:
            return URLPath.addInstructions
        case .removeInstruction:
            return URLPath.removerInstructions
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .feeds, .details, .getinstructions, .getIngredients:
            return .get
        case .add, .addRecipePhoto, .addIngredient, .removeIngredient, .addInstruction, .removeInstruction:
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
}


