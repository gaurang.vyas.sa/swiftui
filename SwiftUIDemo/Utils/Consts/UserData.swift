//
//  UserDefaults.swift
//  My Recipes
//
//  Created by Gaurang Vyas on 28/08/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import Foundation

struct UserData {
    
    @UserDefault("ud_userId", defaultValue: nil)
    static var userId: Int?
    
    @UserDefault("ud_token", defaultValue: "")
    static var token: String
    
    static var isLoggedIn: Bool{
        return !token.isEmpty
    }
    
    static func doLogin(info:LoginInfo){
        userId = info.id
        token = info.token ?? ""
        SceneDelegate.instance.setRootScene()
    }
    
    static func clear(){
        userId = nil
        token = ""
    }
    
    static func logout(){
        clear()
    }
    
}

