//
//  Messages.swift
//  My Recipes
//
//  Created by Gaurang Vyas on 28/08/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import Foundation

enum Messages{
    static let emailAndPasswordAreRequired = "Email and Password are required!"
    static let enterRecipeName = "Enter Recipe Name"
    static let enterDuration = "Enter Duration"
    static let chooseImage = "Choose Image"
    static let enterIngredient = "Enter Ingredient"
    static let enterInstruction = "Enter Instruction"
    static let youDontHaveCamera = "You don't have camera"
    static let yourEmail = "Your Email"
    static let password = "Password"
}
