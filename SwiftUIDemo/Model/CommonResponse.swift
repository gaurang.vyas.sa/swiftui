//
//  CommonResponse.swift
//  My Recipes
//
//  Created by Gaurang Vyas on 02/09/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import Foundation

struct CommonResponse: Codable {
    let msg, photo: String?
    let id: Int?
}
