//
//  RecipeDetailsSections.swift
//  SwiftUIDemo
//
//  Created by Gaurang Vyas on 13/09/19.
//  Copyright © 2019 Gaurang Vyas. All rights reserved.
//

import Foundation

struct RecipeDetailsSections{
    let id = UUID()
    let title: String
    let isIngredients: Bool
    let textArray: [String]
}
